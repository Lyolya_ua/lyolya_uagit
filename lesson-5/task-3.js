/**
 * Задача 6.
 *
 * Создайте функцию `isEven`, которая принимает число качестве аргумента.
 * Функция возвращает булевое значение.
 * Если число, переданное в аргументе чётное — возвращается true.
 * Если число, переданное в аргументе нечётное — возвращается false.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве входного аргумента был передан не числовой тип;
 */

function isEven (number) {
    if(typeof number === 'number' && typeof number !== NaN) {
        if(number % 2 === 0) {
            return true;
        } else {
            return false;
        }
    }

    throw new Error(`${number} - is not a number!`);
}

console.log(isEven(3)); // false
console.log(isEven(4)); // true
