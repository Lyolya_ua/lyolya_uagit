/**
 * Задача 1.
 *
 * Создайте функцию `f`, которая возвращает куб числа, переданного в качестве аргумента.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве аргумента был передан не числовой тип.
 */

function f2 (number) {
    if(typeof number === 'number' && typeof number !== NaN) {
        return number * number * number;
    }

    throw new Error(`${number} - is not a number!`);
}

console.log(f2(2)); // 8
