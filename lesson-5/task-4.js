/**
 * Задача 7.
 *
 * Создайте функцию `getDivisors`, которая принимает число в качестве аргумента.
 * Функция возвращает массив его делителей (чисел, на которое делится данное число начиная от 1 и заканчивая самим собой).
 *
 * Условия:
 * - Генерировать ошибку, если в качестве входного аргумента был передан не числовой тип;
 * - Генерировать ошибку, если в качестве входного аргумента был передано число меньшее, чем 1.
 *
 * Заметки:
 * - В решении вам понадобится использовать цикл с перебором массива.
 */

function getDivisors(number) {
    if(typeof number === 'number' && typeof number !== NaN) {
        if(number < 1) {
            throw new Error(`${number} - must be greater than 1!`);
        }

        const tempArray = new Array(number);
        let array = [];

        for(let i = 1; i <= tempArray.length; i++) {
            if(number % i === 0) {
                array.push(i);
            }
        }

        return array;
    }

    throw new Error(`${number} - is not a number!`);
}

console.log(getDivisors(12)); // [1, 2, 3, 4, 6, 12]
