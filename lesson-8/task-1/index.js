/**
 * Создать форму динамически при помощи JavaScript.
 *
 * В html находится пример формы которая должна быть сгенерирована.
 *
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 *
 * Обязательно!
 * 1. Для генерации элементов обязательно использовать метод document.createElement
 * 2. Для установки атрибутов элементам обязательно необходимо использовать document.setAttribute
 * 3. Всем созданным элементам необходимо добавить классы как в разметке
 * 4. После того как динамическая разметка будет готова необходимо удалить код в HTML который находится между комментариями
 */

(function() {
    const elementsData = {
        labelEmailData: {
            text: 'Электропочта',
            attrubutes: {
                for: 'email'
            }
        },
        inputEmailData: {
            classes: {
                0: 'form-control'
            },
            attrubutes: {
                placeholder: 'Введите свою электропочту',
                type: 'email',
                id: 'email'
            }
        },
        labelPassData: {
            text: 'Пароль',
            attrubutes: {
                for: 'password'
            }
        },
        inputPassData: {
            classList: {
                0: 'form-control'
            },
            attrubutes: {
                placeholder: 'Введите пароль',
                type: 'password',
                id: 'password'
            }
        },
        labelCheckBoxData: {
            text: 'Запомнить меня',
            classList: {
                0: 'form-check-label'
            },
            attrubutes: {
                for: 'exampleCheck1'
            }
        },
        inputCheckBoxData: {
            attrubutes: {
                type: 'checkbox',
                id: 'exampleCheck1'
            },classList: {
                0: 'form-check-input'
            },
        },
        buttonData: {
            text: 'Вход',
            classList: {
                0: 'btn',
                1: 'btn-primary'
            },
            attrubutes: {
                type: 'submit'
            }
        }
    };

    const form = document.getElementById('form');
    const formGrour = document.createElement('div');
    formGrour.classList.add('form-group');

    const formGrourPass = formGrour.cloneNode();
    const formGrourCheck = formGrour.cloneNode();
    formGrourCheck.classList.add('form-check');

    form.appendChild(formGrour);
    form.appendChild(formGrourPass);
    form.appendChild(formGrourCheck);

    const labelEmail = document.createElement('label');
    formGrour.appendChild(labelEmail);

    const inputEmail = document.createElement('input');
    formGrour.appendChild(inputEmail);

    const labelPass = document.createElement('label');
    formGrourPass.appendChild(labelPass);

    const inputPass = document.createElement('input');
    formGrourPass.appendChild(inputPass);

    const inputCheckBox = document.createElement('input');
    formGrourCheck.appendChild(inputCheckBox);

    const labelCheckBox = document.createElement('label');
    formGrourCheck.appendChild(labelCheckBox);

    const button = document.createElement('button');
    form.appendChild(button);

    function createElements(element, obj) {
        for(let i in obj) {
            if(i === 'text') {
                element.textContent = obj[i];
            } else if(i === 'attrubutes') {
                for(let key in obj[i]) {
                    element.setAttribute(key, obj[i][key]);
                }
            } else {
                for(let key in obj[i]) {
                    element.classList.add(obj[i][key]);
                }
            }
        }
    };

    createElements(labelEmail, elementsData.labelEmailData);
    createElements(inputEmail, elementsData.inputEmailData);
    createElements(labelPass, elementsData.labelPassData);
    createElements(inputPass, elementsData.inputPassData);
    createElements(inputCheckBox, elementsData.inputCheckBoxData);
    createElements(labelCheckBox, elementsData.labelCheckBoxData);
    createElements(button, elementsData.buttonData);

})();
