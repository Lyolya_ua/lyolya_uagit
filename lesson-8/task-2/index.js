/**
 * Доработать форму из 1-го задания.
 *
 * Добавить обработчик сабмита формы.
 *
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 *
 * Обязательно!
 * 1. При сабмите формы страница не должна перезагружаться
 * 2. Генерировать ошибку если пользователь пытается сабмитить форму с пустыми или содержащими только пробел(ы) полями.
 * 3. Если поля формы заполнены и пользователь нажимает кнопку Вход → вывести в консоль объект следующего вида
 * {
 *  email: 'эмейл который ввёл пользователь',
 *  password: 'пароль который ввёл пользователь',
 *  remember: 'true/false'
 * }
 */


(function() {
    const elementsData = {
        labelEmailData: {
            text: 'Электропочта',
            attrubutes: {
                for: 'email'
            }
        },
        inputEmailData: {
            classes: {
                0: 'form-control'
            },
            attrubutes: {
                placeholder: 'Введите свою электропочту',
                type: 'email',
                id: 'email'
            }
        },
        labelPassData: {
            text: 'Пароль',
            attrubutes: {
                for: 'password'
            }
        },
        inputPassData: {
            classList: {
                0: 'form-control'
            },
            attrubutes: {
                placeholder: 'Введите пароль',
                type: 'password',
                id: 'password'
            }
        },
        labelCheckBoxData: {
            text: 'Запомнить меня',
            classList: {
                0: 'form-check-label'
            },
            attrubutes: {
                for: 'exampleCheck1'
            }
        },
        inputCheckBoxData: {
            attrubutes: {
                type: 'checkbox',
                id: 'exampleCheck1'
            },classList: {
                0: 'form-check-input'
            },
        },
        buttonData: {
            text: 'Вход',
            classList: {
                0: 'btn',
                1: 'btn-primary'
            },
            attrubutes: {
                type: 'submit'
            }
        }
    };

    const form = document.getElementById('form');
    const formGroup = document.createElement('div');
    formGroup.classList.add('form-group');

    const formGroupPass = formGroup.cloneNode();
    const formGroupCheck = formGroup.cloneNode();
    formGroupCheck.classList.add('form-check');

    form.appendChild(formGroup);
    form.appendChild(formGroupPass);
    form.appendChild(formGroupCheck);

    const labelEmail = document.createElement('label');
    formGroup.appendChild(labelEmail);

    const inputEmail = document.createElement('input');
    formGroup.appendChild(inputEmail);

    const labelPass = document.createElement('label');
    formGroupPass.appendChild(labelPass);

    const inputPass = document.createElement('input');
    formGroupPass.appendChild(inputPass);

    const inputCheckBox = document.createElement('input');
    formGroupCheck.appendChild(inputCheckBox);

    const labelCheckBox = document.createElement('label');
    formGroupCheck.appendChild(labelCheckBox);

    const button = document.createElement('button');
    form.appendChild(button);

    const messageBlock = document.createElement('div');
    form.appendChild(messageBlock);
    messageBlock.style.display = 'none';
    messageBlock.style.padding = '20px';
    messageBlock.style.border = '1px solid';
    messageBlock.style.textAlign = 'center';
    messageBlock.style.margin = '20px 0 0';

    function createElements(element, obj) {
        for(let i in obj) {
            if(i === 'text') {
                element.textContent = obj[i];
            } else if(i === 'attrubutes') {
                for(let key in obj[i]) {
                    element.setAttribute(key, obj[i][key]);

                    if(obj[i][key] === 'email' || obj[i][key] === 'password') {
                        element.setAttribute('name', obj[i][key]);
                        element.setAttribute('value', '');
                    }
                }
            } else {
                for(let key in obj[i]) {
                    element.classList.add(obj[i][key]);
                }
            }
        }
    }

    createElements(labelEmail, elementsData.labelEmailData);
    createElements(inputEmail, elementsData.inputEmailData);
    createElements(labelPass, elementsData.labelPassData);
    createElements(inputPass, elementsData.inputPassData);
    createElements(inputCheckBox, elementsData.inputCheckBoxData);
    createElements(labelCheckBox, elementsData.labelCheckBoxData);
    createElements(button, elementsData.buttonData);

    function sendForm () {
        const object = {};
        const email = inputEmail.value;
        const emailTrim = email.trim();
        const password = inputPass.value;
        const passwordTrim = password.trim();

        function getMessage() {
            messageBlock.innerHTML = 'Form has been sent!';
            messageBlock.style.display = 'block';
            messageBlock.style.color = 'green';
            messageBlock.style.borderColor = 'green';
            inputEmail.style.borderColor = 'rgb(206, 212, 218)';
            inputPass.style.borderColor = 'rgb(206, 212, 218)';
        }

        function getError(input, textError = 'All fields must be filled!') {
            messageBlock.innerHTML = textError;
            messageBlock.style.display = 'block';

            if(input) {
                input.style.borderColor = 'red';
                messageBlock.style.color = 'red';
            } else {
                inputEmail.style.borderColor = 'red';
                inputPass.style.borderColor = 'red';
                messageBlock.style.color = 'red';
            }
        }

        function getEmail (email) {
            let newEmail = email.toLocaleLowerCase();
            console.log(email);
            console.log(newEmail);

            let array = Array.from(newEmail);
            let filter = array.filter(item => {
                return item === '@';
            });

            return filter.join('');
        }

        if(passwordTrim === '' && emailTrim === '') {
            getError();
        } else {
            let trueEmail = getEmail(emailTrim);

            if(trueEmail && passwordTrim !== '') {
                object.email = email;
                object.password = passwordTrim;
                object.remember = inputCheckBox.checked;
                console.log(object);
                getMessage();

                return object;
            } else {
                if(!trueEmail) {
                    getError(inputEmail, "Please include the '@' in the email address");
                } else {
                    getError(inputPass, "You use gaps for password or it is empty!");
                }
            }
        }
    }

    function resetForm() {
        inputEmail.value = '';
        inputPass.value = '';
        inputCheckBox.checked = false;
    }

    button.addEventListener('click', (e) => {
        e.preventDefault();
        sendForm();
        resetForm();
    })
})();
