/**
 * Напишите функцию для форматирования даты.
 *
 * Фукнция принимает 3 аргумента
 * 1. Дата которую необходимо отформатировать
 * 2. Строка которая содержит желаемый формат даты
 * 3. Разделитель для отформтированной даты
 *
 * Обратите внимание!
 * 1. DD день в формате — 01, 02...31
 * 2. MM месяц в формате — 01, 02...12
 * 3. YYYY год в формате — 2020, 2021...
 * 4. Строка которая обозначает формат даты разделена пробелами
 * 5. В качестве разделителя может быть передано только дефис, точка или слеш
 * 6. Генерировать ошибку если в формате даты присутствет что-то другое кроме DD, MM, YYYY
 * 7. 3-й аргумент опциональный, если он передан не был, то в качестве разделителя используется точка
 */

const formatDate = (date, format, delimiter = '.') => {
    const obj = {
        'YYYY': function(date) {
            return date.getFullYear();
        },
        'MM': function(date) {
            return date.getMonth();
        },
        'DD': function(date) {
            return date.getDate();
        }
    };

    const formatArray = format.split(' ');
    let newArray = [];
    let final = '';

    if(delimiter === '.' || delimiter === '/' || delimiter === '-') {
        newArray = formatArray.map(item => {
            if(obj[item]) {
                return obj[item](date);
            }

            throw new Error('You use incorrect format of date.');
        });

        if(newArray.length > 1) {
            final = newArray.join(delimiter);
        } else {
            final = newArray.join();
        }

        return final;
    } else {
        throw new Error(`<<${delimiter}>> must be '.' or '/' or '-'`)
    }
};

console.log(formatDate(new Date(2021, 10, 22), 'DD MM YYYY', '/')); // 22/10/2021
console.log(formatDate(new Date(2021, 10, 22), 'DD MM', '.')); // 22.10
console.log(formatDate(new Date(2021, 10, 22), 'YYYY', '.')); // 2021
