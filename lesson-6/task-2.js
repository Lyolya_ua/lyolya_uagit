function calculateAdvanced(...rest) {
    let object = {
        value: 0,
        errors: [],
    };

    rest.forEach((cb, i) => {
        if(typeof cb !== 'function') {
            throw new Error(`<< ${cb} >> is nor a function!`)
        }

        try{
            let temp = cb(object.value);

            if(temp === undefined) {
                object.errors.push({
                    index: i,
                    name: 'Error',
                    message: 'callback at index 0 did not return any value.'
                })
            } else {
                object.value = temp;
            }
        } catch(error) {
            object.errors.push({
                index: i,
                name: error.name,
                message: error.message,
            });
        }
    });

    return object;
}

const result = calculateAdvanced(
    () => {},
    () => {
        return 7;
    },
    () => {},
    prevResult => {
        return prevResult + 4;
    },
    () => {
        throw new RangeError('Range is too big.');
    },
    prevResult => {
        return prevResult + 1;
    },
    () => {
        throw new ReferenceError('ID is not defined.');
    },
    prevResult => {
        return prevResult * 5;
    },
);

console.log(result);

// Функция вернёт:
// { value: 60,
//     errors:
//      [ { index: 0,
//          name: 'Error',
//          message: 'callback at index 0 did not return any value.' },
//        { index: 2,
//          name: 'Error',
//          message: 'callback at index 2 did not return any value.' },
//        { index: 4, name: 'RangeError', message: 'Range is too big.' },
//        { index: 6,
//          name: 'ReferenceError',
//          message: 'ID is not defined.' } ] }
