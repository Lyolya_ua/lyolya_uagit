/**
 * Задача 3.
 *
 * Напишите функцию `createArray`, которая будет создавать массив с заданными значениями.
 * Первым параметром функция принимает значение, которым заполнять массив.
 * А вторым — количество элементов, которое должно быть в массиве.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента были переданы не число, не строка, не объект и не массив;
 * - В качестве второго аргумента был передан не число.
 */

// Решение

const result = createArray('x', 5);

function createArray(arr, length) {
    if( arr !== null || arr === undefined || arr !== NaN) {
        if( typeof length !== 'number' ) {
            throw new Error(`${length} - should be a number`);
        } else {
            let array = new Array(length).fill(arr);
            return array;
        }
    } else {
        throw new Error(`${arr} should be a number, string, array or object`);
    }
}

console.log(result); // [ x, x, x, x, x ]
