/**
 * Задача 2.
 *
 * Вручную создать имплементацию функции `filter`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенный метод Array.prototype.filter использовать запрещено.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция.
 *
 * Заметки:
 * - Второй аргумент встроенного метода filter (thisArg) имплементировать не нужно.
 */

const array = ['Доброе утро!', 'Добрый вечер!', 'Добрый вечер!', 3, 512, '#', 'До свидания!'];

function callback(item) {
    return item === 'Добрый вечер!';
}

function filter(arr, cb) {
    if(!Array.isArray(arr)) {
        throw new Error(`${arr} - is not an array!`);
    }
    if( typeof cb !== 'function') {
        throw new Error(`${cb} - is not a function!`);
    }

    let newArray = [];

    arr.forEach(item => {
        if(cb(item)){
            newArray.push(item);
        }
    });

    return newArray;
}

const filteredArray = filter(array, callback);

console.log(filteredArray); // ['Добрый вечер!']
