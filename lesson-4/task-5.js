/**
 * Задача 5.
 *
 * Вручную создать имплементацию функции `reduce`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенные методы Array.prototype.reduce и Array.prototype.reduceRight использовать запрещено;
 * - Третий аргумент функции reduce является не обязательным;
 * - Если третий аргумент передан — он станет начальным значением аккумулятора;
 * - Если третий аргумент не передан — начальным значением аккумулятора станет первый элемент обрабатываемого массива.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция;
 */

const array = [1, 2, 3, 4, 5];
const INITIAL_ACCUMULATOR = 6;

function callback(accumulator, item) {
    console.log('item - ', item);
    console.log('accumulator - ', accumulator);
    return accumulator + item;
}

function reduce(arr, cb, accamulator = 0) {
    if(!Array.isArray(arr)) {
        throw new Error(`${arr} - is not an array!`);
    }
    if( typeof cb !== 'function') {
        throw new Error(`${cb} - is not a function!`);
    }

    array.forEach(item => {
        accamulator = cb(accamulator, item);
    });

    return accamulator;
}

const result = reduce(array, callback, INITIAL_ACCUMULATOR);

console.log(result); // 21
