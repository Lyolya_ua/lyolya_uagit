/**
 * Задача 2.
 *
 * Исправить функцию checkSpam(source, spam)
 * Функция возвращает true, если строка source содержит подстроку spam. Иначе — false.
 *
 * Условия:
 * - Функция принимает два параметра;
 * - Функция содержит валидацию входных параметров на тип string.
 * - Функция должна быть нечувствительна к регистру
 */

// РЕШЕНИЕ
function checkSpam(source, spam) {
    const a = source.toLowerCase();
    const b = spam.toLowerCase();
    const result = a.indexOf(b);

    if( result > 0 ) {
        return true;
    } else {
        return false;
    }
}

console.log(checkSpam('pitterXXX@gmail.com', 'xxx')); // true
console.log(checkSpam('pitterxxx@gmail.com', 'XXX')); // true
console.log(checkSpam('pitterxxx@gmail.com', 'sss')); // false
