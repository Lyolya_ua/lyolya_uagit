/**
 * Задача 1.
 *
 * Исправить функцию upperCaseFirst(str).
 * Функция преобразовывает первый символ переданной строки в заглавный и возвращает новую строку.
 *
 * Условия:
 * - Функция принимает один параметр;
 * - Необходимо проверить что параметр str является строкой
 */

// РЕШЕНИЕ
function upperCaseFirst(str) {
    if(typeof str === 'string') {
        const firstLetter = str.charAt(0).toUpperCase();
        const result = str.slice(1);

        return firstLetter + result;
    } else {
        return `${str} is not a string`;
    }
}

console.log(upperCaseFirst('pitter')); // Pitter
console.log(upperCaseFirst('')); // ''
